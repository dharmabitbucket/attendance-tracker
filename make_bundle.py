#!/usr/bin/env python3
import os
import sys
import time
from string import Template
from datetime import datetime
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


class event_handler(FileSystemEventHandler):
    def __init__(self):
        self.files = [
            "external/accordion.js",
            "external/Chart.bundle.min.js",
            "external/select.js",
            "lib/Firebase.js",
            "lib/Router.js",
            "lib/Component.js",
            "lib/utils.js",
            "Main.js",
            "components/Select.js",
            "components/Input.js",
            "components/Form.js",
            "components/SubmitButton.js",
            "screens/Home.js",
            "screens/Login.js",
            "screens/Register.js",
            "screens/Dashboard.js",
            "screens/Courses.js",
            "screens/TeacherDashboard.js",
            "screens/TeacherSession.js",
        ]
        self.output_file = 'bundle.js'

    def create_description(self, path, name):
        stat = os.stat(path)
        date = datetime.utcfromtimestamp(stat.st_mtime).strftime('%Y-%m-%dT%H:%M:%SZ')
        return Template("""

/*
* $name
* Last modified: $date
*/

    """).substitute(name = name, date = date)

    def dispatch(self, event):
        with open(self.output_file, 'w+') as output:
            output.write(self.create_description(self.output_file, self.output_file))
            for file in self.files:
                path = base + file
                description = self.create_description(path, file)
                data = ''
                with open(path, 'r') as current:
                    data = current.read()
                output.write(description)
                output.write(data)
        print('Built bundle at', datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'))



if __name__ == "__main__":
    base = './js/'
    handler = event_handler()
    handler.dispatch(None)
    observer = Observer()
    observer.schedule(handler, base, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
