class Router {
  constructor () {
    const { history } = window
    const pushState = history.pushState
    const back = history.back
    const actions = { push: 'PUSH', pop: 'POP' }

    const stateChanged = (
      action,
      route = this.prevState.route,
      params = this.state.params
    ) => {
      try {
        if (typeof this.routes[route].callback === 'function') {
          this.prevState = { ...this.state }
          if (action === actions.push) {
            this.state = { route, title: this.routes[route].title, params }
            this.stack = [...this.stack, this.state]
          } else {
            this.state = this.stack.pop()
            this.state.params = params
          }
          document.title = this.state.title
          this.routes[route].callback(this.prevState, this.state)
          return true
        } else {
          console.error(`Route ${route} might not have been defined!`)
          return false
        }
      } catch (error) {
        console.error(`Route ${route} threw an error!`, error)
        return false
      }
    }

    history.back = params => {
      const result = back.apply(history)
      stateChanged(actions.pop, undefined, params)
      return result
    }

    history.pushState = (route, params) => {
      const result = pushState.apply(history, [params, route, route])
      stateChanged(actions.push, route, params)
      return result
    }

    window.addEventListener('popstate', event => stateChanged(
      actions.push, (/\w+$/.test(event.target.location.pathname) && event.target.location.pathname.match(/\w+$/)[0]) || 'login', event.state
    ))

    this.stack = []
    this.prevState = {}
    this.state = {}
    this.routes = {}
  }

  getCurrentRoute () {
    return this.state
  }

  push (route, params) {
    const { history } = window
    history.pushState(route, params)
  }

  back (params) {
    const { history } = window
    return history.back(params)
  }

  appendRoute ({ route, title, callback }) {
    this.routes = {
      ...this.routes,
      [route]: {
        title,
        callback
      }
    }
  }
}
