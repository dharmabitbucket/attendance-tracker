const firebaseInit = async () => {
  var config = {
    apiKey: 'AIzaSyCy88AUq8YQ8ApatFMuRcKlEtwZ6m64a6Y',
    authDomain: 'attendnet.firebaseapp.com',
    databaseURL: 'https://attendnet.firebaseio.com',
    projectId: 'attendnet',
    storageBucket: '',
    messagingSenderId: '762972528851'
  }
  firebase.initializeApp(config)

  firebase.auth().onAuthStateChanged(async user => {
    if (user) {
      document.cookie = firebase.auth().currentUser !== null
        ? `__session=${await firebase.auth().currentUser.getIdToken(true)};max-age=600000`
        : `__session=` 
      const snapshot = await firebase
        .database()
        .ref('users/' + user.uid)
        .once('value')
      switch (snapshot.val().usertype) {
        case 1:
        case '1':
          router.push('dashboard', {})
          break
        case 2:
        case '2':
          router.push('teacher_dashboard', {})
          break
      }
    } else {
      router.push('login', {})
    }
  })
}

const listenForFirebaseStateChange = async () =>
  firebase.auth().onAuthStateChanged(async user => {
    if (user) {
      document.cookie = firebase.auth().currentUser !== null
        ? `__session=${await firebase.auth().currentUser.getIdToken(true)};max-age=600000`
        : `__session=`
      const snapshot = await firebase
        .database()
        .ref('users/' + user.uid)
        .once('value')
      switch (snapshot.val().usertype) {
        case 1:
        case '1':
          router.push('dashboard', {})
          break
        case 2:
        case '2':
          router.push('teacher_dashboard', {})
          break
      }
    } else {
      router.push('login', {})
    }
  })
