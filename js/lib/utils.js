const logout = () => {
  firebase.auth().signOut()
  document.cookie = '__session='
  router.push('login', {})
}

const URL = 'http://localhost:5000/'