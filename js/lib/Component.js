class Component {
  constructor ({ type, parent = null, attributes = {}, state = {}, bindings = {} }) {
    this.key = 'JustALongHashKey'
      .split('')
      .map(
        () => 'abcdefghijkmlnopqrstuvwxyz0123456789'[(Math.random() * 36) | 0]
      )
      .join('')
    this.parent = parent
    this.children = []
    this.element = null
    this.type = type
    this.attributes = attributes
    this.bindings = bindings
    this.state = state

    if (this.parent) {
      this.parent.appendChild(this)
    }

    this.create()
    this.componentDidCreate()
  }

  setParent (parent) {
    this.parent = parent
    return this
  }

  appendChild (component) {
    this.children = [...this.children, component]
    return this
  }

  removeChild (component) {
    this.children = this.children.filter(item => item.id !== component.id)
    return this
  }

  setAttributes (attributes) {
    if (this.shouldComponentUpdate()) {
      this.attributes = {
        ...this.attributes,
        ...attributes
      }
      this.create()
      this.componentDidUpdate()
    }
    return this
  }

  setState (state) {
    if (this.shouldComponentUpdate()) {
      this.state = {
        ...this.state,
        ...state
      }
      this.create()
      this.componentDidUpdate()
    }
    return this
  }

  setBindings (bindings) {
    if (this.shouldComponentUpdate()) {
      this.bindings = {
        ...this.bindings,
        ...bindings
      }
      this.create()
      this.componentDidUpdate()
    }
  }

  shouldComponentUpdate () {
    return true
  }

  create () {
    const element = this.element || document.createElement(this.type)

    Object.keys(this.bindings).forEach(key => {
      this.attributes[key] = this.state[this.bindings[key]]
    })
    Object.keys(this.attributes).forEach(key => {
      element[key] = this.attributes[key]

        // element.setAttribute("iabadaba", "chinci")
        // console.log("key:", key)
        // console.log("value:", this.attributes[key])
        if( key === "itemprop" || key === "itemscope" || key ==="itemtype"){
        element.setAttribute(key, this.attributes[key])
        }
    })

    this.children.forEach(child => child.create())
    if (!this.element) {
      if (this.parent && this.parent.element) {
        this.parent.element.appendChild(element)
      } else {
        document.body.append(element)
      }
    } else {
      if (
        this.parent &&
        this.parent.element &&
        ![...this.parent.element.children].find(item => item === element)
      ) {
        this.parent.element.appendChild(element)
      }
    }

    this.element = element
    return this.element
  }

  componentDidCreate () {}

  componentDidUpdate () {}
}
