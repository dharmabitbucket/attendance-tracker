const Dashboard = (() => {
  const route = 'dashboard'
  const title = 'Dashboard'

  let classesCardBodyUl = null

  const fetchCourses = async () => {
    return fetch(`/courses`, {
      method: 'GET',
      mode: 'cors',
      credentials: 'include',
    }).then(data => data.json())
  }

  const displaySession = ({ info, fail }) => {
    if (fail) return

    classesCardBodyUl.element.innerHTML = ''

    info.forEach(course => {
      let attendanciesAndGradesById = {}
      Object.keys(course.sessions).forEach(key => {
        if (key === '0') return
        Object.keys(course.sessions[key].attendents_id).forEach(userKey => {
          const user = course.sessions[key].attendents_id[userKey]
          if (user === 0 || user === '0') return
          attendanciesAndGradesById = {
            ...attendanciesAndGradesById,
            [user.id]: {
              email: user.email,
              attendancies: attendanciesAndGradesById[user.id] && attendanciesAndGradesById[user.id].attendancies !== undefined
                ? attendanciesAndGradesById[user.id].attendancies + 1
                : 1,
              grades: []
            }
          }
        })
      })
      Object.keys(course.grades).forEach(userKey => {
        const user = course.grades[userKey]
        if (user === 0 || user === '0') return
          attendanciesAndGradesById = {
          ...attendanciesAndGradesById,
          [user.student_id]: {
            ...attendanciesAndGradesById[user.student_id],
            grades: attendanciesAndGradesById[user.student_id] && attendanciesAndGradesById[user.student_id].grades !== undefined
              ? [ ...attendanciesAndGradesById[user.student_id].grades, user.value ]
              : [ user.value ],
            attendancies: attendanciesAndGradesById[user.student_id] && attendanciesAndGradesById[user.student_id].attendancies !== undefined
              ? attendanciesAndGradesById[user.student_id].attendancies
              : 0,
            email: attendanciesAndGradesById[user.student_id] && attendanciesAndGradesById[user.student_id].email !== undefined
              ? attendanciesAndGradesById[user.student_id].email
              : 'anonymous@person.net',
          }
        }
      })

      Object.keys(attendanciesAndGradesById).forEach(key => {
        const classContainer = new Component({ type: 'li', attributes: { className: 'list-element' }, parent: classesCardBodyUl })
        const accordion = new Component({ type: 'div', attributes: { className: 'accordion' }, parent: classContainer })
        new Component({ type: 'span', attributes: { className: 'bolder', innerHTML: course.class_name }, parent: accordion })
        new Component({ type: 'span', attributes: { innerHTML: ` with ${course.teacher_email}` }, parent: accordion })
        
        const panel = new Component({ type: 'div', attributes: { className: 'accordion' }, parent: classContainer })
        const attendanciesContainer = new Component({ type: 'div', parent: panel })
        new Component({ type: 'span', attributes: { innerHTML: 'Attendances until now: ' }, parent: attendanciesContainer })
        new Component({ type: 'span', attributes: { className: 'bolder', innerHTML: attendanciesAndGradesById[key].attendancies }, parent: attendanciesContainer })
        const gradesContainer = new Component({ type: 'div', parent: panel })
        new Component({ type: 'span', attributes: { innerHTML: 'Grades until now: ' }, parent: gradesContainer })
        new Component({ type: 'span', attributes: { className: 'bolder', innerHTML: attendanciesAndGradesById[key].grades.join(', ') || '-' }, parent: gradesContainer })
      })
    })
  }

  const submitCallback = async ({ code }) => {
    await fetch(`/addattendence`, {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({ 'session_id': code })
    }).then(data => data.json())
  }

  const render = async () => {
    document.body.innerHTML = ''

    const box = new Component({ type: 'div', attributes: { className: 'wrapper', '@context': 'http://schema.org', '@type': 'Event' } })
    const navbar = new Component({ type: 'div', attributes: { className: 'navbar', itemscope: '', itemtype: 'http://www.schema.org/SiteNavigationElement' }, parent: box })
    const navbarLogoUl1 = new Component({ type: 'ul', attributes: { className: 'ul-left logo', itemscope:' ', itemtype: 'http://schema.org/Organization' }, parent: navbar })
    const navbarLogoLi1 = new Component({ type: 'li', parent: navbarLogoUl1 })
    const navbarLogoLiA1 = new Component({ type: 'a', attributes: { onclick: () => router.push('dashboard', {}), itemprop: 'url' }, parent: navbarLogoLi1 })
    new Component({ type: 'span', attributes: { innerHTML: 'Attend' }, parent: navbarLogoLiA1 })
    new Component({ type: 'span', attributes: { className: 'text-blue', innerHTML: 'N' }, parent: navbarLogoLiA1 })
    new Component({ type: 'span', attributes: { innerHTML: 'et' }, parent: navbarLogoLiA1 })
    const navbarLogoUl2 = new Component({ type: 'ul', parent: navbar })
    const navbarLogoLi2 = new Component({ type: 'li', parent: navbarLogoUl2 })
    new Component({ type: 'a', attributes: { itemprop: 'url', innerHTML: 'Dash', onclick: () => router.push('dashboard', {}) }, parent: navbarLogoLi2 })
    const navbarLogoLi3 = new Component({ type: 'li', parent: navbarLogoUl2 })
    new Component({ type: 'a', attributes: { itemprop: 'url', innerHTML: 'Logout', onclick: logout }, parent: navbarLogoLi3 })

    const classContainer = new Component({ type: 'div', attributes: { className: 'container container-center' }, parent: box })
    const classCard = new Component({ type: 'div', attributes: { className: 'card card-classes' }, parent: classContainer })
    const classCardHeader = new Component({ type: 'div', attributes: { className: 'card-header' }, parent: classCard })
    new Component({ type: 'h3', attributes: { className: 'h3', innerHTML: 'Courses' }, parent: classCardHeader })
    new Component({ type: 'hr', parent: classCard })
    const classCardBody = new Component({ type: 'div', attributes: { className: 'card-body' }, parent: classCard })
    classesCardBodyUl = new Component({ type: 'ul', attributes: { className: 'list-elements' }, parent: classCardBody })

    const container1 = new Component({ type: 'div', attributes: { className: 'container container-center' }, parent: box })
    const codeContainer = new Component({ type: 'div', attributes: { className: 'card card-code' }, parent: container1 })
    const codeContainerHeader = new Component({ type: 'div', attributes: { className: 'card-header' }, parent: codeContainer })
    new Component({ type: 'h3', attributes: { className: 'h3', innerHTML: 'Attendance' }, parent: codeContainerHeader })
    new Component({ type: 'hr', parent: codeContainer })
    const codeContainerBody = new Component({ type: 'div', attributes: { className: 'card-body' }, parent: codeContainer })
    const form = new Form({ attributes: { method: 'POST' }, parent: codeContainerBody })
    new Component({ type: 'label', attributes: { for: 'code_field', className: 'text-label', innerHTML: 'Enter Attendance Code' }, parent: form })
    new Input({ attributes: { id: 'code_field', type: 'text', name: 'code', placeholder: 'E.g: #4F65' }, parent: form })
    new SubmitButton({ attributes: { value: 'Submit', itemtype:'http://schema.org/JoinAction' }, parent: form }, submitCallback)

    const courses = await fetchCourses()
    displaySession(courses)
  }

  router.appendRoute({
    route,
    title,
    callback: render
  })

  return { render }
})()
