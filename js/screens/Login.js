const Login = (() => {
  const route = 'login'
  const title = 'Login'

  const submitCallback = async (data) => {
    const { email, password } = data
    try {
      await firebase.auth().signInWithEmailAndPassword(email, password)
      listenForFirebaseStateChange()
    } catch (error) {
      console.error(error)
      alert(error.message)
    }
  }

  const render = () => {
    document.body.innerHTML = ''

    const box = new Component({ type: 'div', attributes: { className: 'container', '@context': 'http://schema.org' } })
    const logo = new Component({ type: 'div', attributes: { className: 'h1 logo', itemscope:'', itemtype:'http://schema.org/Organization' }, parent: box })
    new Component({ type: 'span', attributes: { innerHTML: 'Attend' }, parent: logo })
    new Component({ type: 'span', attributes: { className: 'text-blue', innerHTML: 'N' }, parent: logo })
    new Component({ type: 'span', attributes: { className: 'h1 logo', innerHTML: 'et' }, parent: logo })

    const loginForm = new Form({ attributes: { id: 'login_div', className: 'main-div', style: 'display: block', method: 'POST' }, parent: box }, submitCallback)
    new Input({ attributes: { type: 'email', name: 'email', placeholder: 'E-mail' }, parent: loginForm })
    new Input({ attributes: { type: 'password', name: 'password', placeholder: 'Password' }, parent: loginForm })
    new SubmitButton({ attributes: { value: 'Login' }, parent: loginForm }, submitCallback)

    const registerParagraph = new Component({ type: 'p', attributes: { innerHTML: "Don't have an account? " }, parent: loginForm })
    new Component({ type: 'a', attributes: { innerHTML: 'Register', onclick: () => router.push('register', {}) }, parent: registerParagraph })
  }

  router.appendRoute({
    route,
    title,
    callback: render
  })

  return { render }
})()
