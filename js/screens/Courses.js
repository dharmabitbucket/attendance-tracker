const Courses = (() => {
  const route = 'courses'
  const title = 'Courses'

  const fetchClasses = async () => {
    //TODO: implement this
    return Promise.resolve()
  }

  const render = async () => {
    document.body.innerHTML = ''

    const box = new Component({ type: 'div', attributes: { className: 'wrapper', '@context': 'http://schema.org' } })
    const navbar = new Component({ type: 'div', attributes: { className: 'navbar', itemscope:'', itemtype: 'http://www.schema.org/SiteNavigationElement' }, parent: box })
    const navbarLogoUl1 = new Component({ type: 'ul', attributes: { className: 'ul-left logo', itemscope: '', itemtype: 'http://schema.org/Organization' }, parent: navbar })
    const navbarLogoLi1 = new Component({ type: 'li', parent: navbarLogoUl1 })
    const navbarLogoLiA1 = new Component({ type: 'a', attributes: { onclick: () => router.push('dashboard', {}), itemprop: 'url' }, parent: navbarLogoLi1 })
    new Component({ type: 'span', attributes: { innerHTML: 'Attend' }, parent: navbarLogoLiA1 })
    new Component({ type: 'span', attributes: { className: 'text-blue', innerHTML: 'N' }, parent: navbarLogoLiA1 })
    new Component({ type: 'span', attributes: { innerHTML: 'et' }, parent: navbarLogoLiA1 })
    const navbarLogoUl2 = new Component({ type: 'ul', parent: navbar })
    const navbarLogoLi2 = new Component({ type: 'li', parent: navbarLogoUl2 })
    new Component({ type: 'a', attributes: { itemprop: 'url',  innerHTML: 'Dash', onclick: () => router.push('dashboard', {}) }, parent: navbarLogoLi2 })
    const navbarLogoLi3 = new Component({ type: 'li', parent: navbarLogoUl2 })
    new Component({ type: 'a', attributes: { itemprop: 'url', innerHTML: 'Classes', onclick: () => router.push('classes', {}) }, parent: navbarLogoLi3 })
    const navbarLogoLi4 = new Component({ type: 'li', parent: navbarLogoUl2 })
    new Component({ type: 'a', attributes: { innerHTML: 'Logout', onclick: logout }, parent: navbarLogoLi4 })

    const container1 = new Component({ type: 'div', attributes: { className: 'container container-center' }, parent: box })
    const classesContainer = new Component({ type: 'div', attributes: { className: 'card card-classes' }, parent: container1 })
    const classesContainerHeader = new Component({ type: 'div', attributes: { className: 'card-header' }, parent: classesContainer })
    const classHeaderValue = new Component({ type: 'h3', attributes: { className: 'h3', innerHTML: 'Classes', itemscope: '', itemtype: 'http://schema.org/CourseInstance' }, parent: classesContainerHeader })
    new Component({Type: 'span', attributes:{className: 'h3', innerHTML: 'Professor Name', itemprop: 'instructor' }, parent: classHeaderValue})
    new Component({ type: 'hr', parent: classesContainer })
    const classesContainerBody = new Component({ type: 'div', attributes: { className: 'card-body' }, parent: classesContainer })
    const classesContainerBodyUl = new Component({ type: 'ul', attributes: { className: 'list-elements' }, parent: classesContainerBody })

    const classes = await fetchClasses()
  }

  router.appendRoute({
    route,
    title,
    callback: render
  })

  return { render }
})()
