const TeacherDashboard = (() => {
  const route = 'teacher_dashboard'
  const title = 'TeacherDashboard'

  let classesCardBodyUl = null

  const fetchClasses = async () => {
    return fetch(`/teacherdashboard`, {
      method: 'GET',
      mode: 'cors',
      credentials: 'include',
    }).then(data => data.json())
  }

  const submitCallback = async data => {
    await fetch(`/createclass`, {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({ 'class': data.class })
    }).then(data => data.json())

    const classes = await fetchClasses()
    classesCardBodyUl.element.innerHTML = ''
    displayClasses(classes)
  }

  const navToSession = (course, sessionId) => async () => {
    router.push('teacher_session', { 'class': course, sessionId })
  }

  const createSession = (course) => async () => {
    const session = await fetch(`/createsession`, {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({ 'class': course })
    }).then(data => data.json())

    router.push('teacher_session', { ...session })
  }

  const displayClasses = ({ classes, fail }) => {
    if (fail) return

    Object.keys(classes).forEach(key => {
      if (key === '0') return
      const classContainer = new Component({ type: 'li', attributes: { className: 'list-element' }, parent: classesCardBodyUl })
      const accordion = new Component({ type: 'div', attributes: { className: 'accordion' }, parent: classContainer })
      new Component({ type: 'span', attributes: { className: 'bolder', innerHTML: key }, parent: accordion })

      const panel = new Component({ type: 'div', attributes: { className: 'panel' }, parent: classContainer })
      Object.keys(classes[key].sessions).forEach(sessionKey => {
        if (sessionKey === '0') return
        session = classes[key].sessions[sessionKey]

        const spanContainer = new Component({ type: 'div', parent: panel })
        new Component({ type: 'span', attributes: { className: 'card-link', onclick: navToSession(key, sessionKey), innerHTML: 'Session '  }, parent: spanContainer })
        new Component({ type: 'span', attributes: { innerHTML: 'on ' }, parent: spanContainer })
        new Component({ type: 'span', attributes: { className: 'bolder', innerHTML: session.data }, parent: spanContainer })
        new Component({ type: 'span', attributes: { innerHTML: `, ${Object.keys(session['attendents_id']).length - 1} attendants` }, parent: spanContainer })
      })
      new SubmitButton({ attributes: { value: 'Start new session' }, parent: panel }, createSession(key))
    })
  }

  const render = async () => {
    document.body.innerHTML = ''

    const box = new Component({ type: 'div', attributes: { className: 'wrapper', '@context': 'http://schema.org' } })
    const navbar = new Component({ type: 'div', attributes: { className: 'navbar',  itemscope:'', itemtype:'http://www.schema.org/SiteNavigationElement' }, parent: box })
    const navbarLogoUl1 = new Component({ type: 'ul', attributes: { className: 'ul-left logo', itemscope:'', itemtype:'http://schema.org/Organization' }, parent: navbar })
    const navbarLogoLi1 = new Component({ type: 'li', parent: navbarLogoUl1 })
    const navbarLogoLiA1 = new Component({ type: 'a', attributes: { itemprop: 'url', onclick: () => router.push('teacher_dashboard', {}) }, parent: navbarLogoLi1 })
    new Component({ type: 'span', attributes: { innerHTML: 'Attend' }, parent: navbarLogoLiA1 })
    new Component({ type: 'span', attributes: { className: 'text-blue', innerHTML: 'N' }, parent: navbarLogoLiA1 })
    new Component({ type: 'span', attributes: { innerHTML: 'et' }, parent: navbarLogoLiA1 })
    const navbarLogoUl2 = new Component({ type: 'ul', parent: navbar })
    const navbarLogoLi2 = new Component({ type: 'li', parent: navbarLogoUl2 })
    new Component({ type: 'a', attributes: {itemprop: 'url', innerHTML: 'Dash', onclick: () => router.push('teacher_dashboard', {}) }, parent: navbarLogoLi2 })
    const navbarLogoLi3 = new Component({ type: 'li', parent: navbarLogoUl2 })
    new Component({ type: 'a', attributes: {itemprop: 'url', innerHTML: 'Logout', onclick: logout }, parent: navbarLogoLi3 })

    const classesContainer = new Component({ type: 'div', attributes: { className: 'container container-center' }, parent: box })
    const classesCard = new Component({ type: 'div', attributes: { className: 'card card-classes', itemscope:'', itemtype: 'http://schema.org/CourseInstance' }, parent: classesContainer })
    const classesCardHeader = new Component({ type: 'div', attributes: { className: 'card-header' }, parent: classesCard })
    new Component({ type: 'h3', attributes: { className: 'h3', innerHTML: 'Classes' }, parent: classesCardHeader })
    new Component({ type: 'hr', parent: classesCard })
    const classesCardBody = new Component({ type: 'div', attributes: { className: 'card-body' }, parent: classesCard })
    classesCardBodyUl = new Component({ type: 'ul', attributes: { className: 'list-elements' }, parent: classesCardBody })

    const container2 = new Component({ type: 'div', attributes: { className: 'container container-center' }, parent: box })
    const newClassCard = new Component({ type: 'div', attributes: { className: 'card card-classes' }, parent: container2 })
    const newClassBodyHeader = new Component({ type: 'div', attributes: { className: 'card-header' }, parent: newClassCard })
    new Component({ type: 'h3', attributes: { className: 'h3', innerHTML: 'Create new class' }, parent: newClassBodyHeader })
    new Component({ type: 'hr', parent: newClassCard })
    const newClassCardBody = new Component({ type: 'div', attributes: { className: 'card-body' }, parent: newClassCard })
    const form = new Form({ attributes: { method: 'POST' }, parent: newClassCardBody })
    new Component({ type: 'label', attributes: { for: 'class_field', className: 'text-label', innerHTML: 'Enter Class Name' }, parent: form })
    new Input({ attributes: { id: 'class_field', type: 'text', name: 'class', placeholder: 'E.g: Science' }, parent: form })
    new SubmitButton({ attributes: { value: 'Submit', '@type':'CreateAction' }, parent: form }, submitCallback)

    const classes = await fetchClasses()
    displayClasses(classes)
  }

  router.appendRoute({
    route,
    title,
    callback: render
  })

  return { render }
})()
