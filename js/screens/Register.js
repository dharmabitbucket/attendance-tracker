const Register = (() => {
  const route = 'register'
  const title = 'Register'

  const submitCallback = async (data) => {
    const { email, password, usertype } = data
    try {
      const { user } = await firebase.auth().createUserWithEmailAndPassword(email, password)
      firebase.database().ref('users/' + user.uid).set({
        usertype,
        classes: { '0': 0 }
      })
      listenForFirebaseStateChange()
    } catch (error) {
      console.error(error)
      alert(error.message)
    }
  }

  const render = (prevState, state) => {
    document.body.innerHTML = ''

    const box = new Component({ type: 'div', attributes: { className: 'container', '@context': 'http://schema.org' } })
    const logo = new Component({ type: 'div', attributes: { className: 'h1 logo',itemscope:'', itemtype:'http://schema.org/Organization' }, parent: box })
    new Component({ type: 'span', attributes: { innerHTML: 'Attend' }, parent: logo })
    new Component({ type: 'span', attributes: { className: 'text-blue', innerHTML: 'N' }, parent: logo })
    new Component({ type: 'span', attributes: { className: 'h1 logo', innerHTML: 'et' }, parent: logo })

    const registerForm = new Form({ attributes: { id: 'register_div', className: 'main-div', style: 'display: block', method: 'POST' }, parent: box }, submitCallback)
    new Input({ attributes: { type: 'email', name: 'email', placeholder: 'E-mail' }, parent: registerForm })
    new Input({ attributes: { type: 'password', name: 'password', placeholder: 'Password' }, parent: registerForm })
    const select = new Select({ attributes: { name: 'usertype', placeholder: 'Password' }, parent: registerForm })
    new Component({ type: 'option', attributes: { innerHTML: 'Attendee', value: 1 }, parent: select })
    new Component({ type: 'option', attributes: { innerHTML: 'Teacher', value: 2 }, parent: select })
    new SubmitButton({ attributes: { value: 'Register', itemtype:'http://schema.org/RegisterAction' }, parent: registerForm }, submitCallback)

    const registerParagraph = new Component({ type: 'p', attributes: { innerHTML: 'Have an account? ' }, parent: registerForm })
    new Component({ type: 'a', attributes: { innerHTML: 'Login', onclick: () => router.push('login', {}) }, parent: registerParagraph })
  }

  router.appendRoute({
    route,
    title,
    callback: render
  })

  return { render }
})()
