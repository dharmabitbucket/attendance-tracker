const Home = (() => {
  const route = '/'
  const title = 'Home'

  const render = () => {
    document.body.innerHTML = ''
    const container = new Component({ type: 'div', attributes: { className: 'container-centered' } })
    new Component({ type: 'h1', attributes: { innerHTML: 'Loading...' }, parent: container })
  }

  router.appendRoute({
    route,
    title,
    callback: render
  })

  router.push(route, {})

  return { render }
})()
