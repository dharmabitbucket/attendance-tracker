const teacher_session = (() => {
  const route = 'teacher_session'
  const title = 'TeacherDashboard'

  let clock = null
  let attendants = null
  let classCardBodyUl = null

  const addGradeCallback = (user, className, session) => async ({ grade }) => {
    await fetch(`/addgrade`, {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({
        'class': className,
        'session_id': session,
        user,
        grade
      })
    }).then(data => data.json())
  }

  const kickCallback = (user, className, session) => async () => {
    await fetch(`/kickstudent`, {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({
        'class': className,
        'session_id': session,
        user
      })
    }).then(data => data.json())
  }

  const fetchSession = async className => {
    return fetch(`/teachersession?class=${className}`, {
      method: 'GET',
      mode: 'cors',
      credentials: 'include',
    }).then(data => data.json())
  }

  const displaySession = (className, session, { grades, sessions, fail }) => {
    if (fail) return

    session = session || Object.keys(sessions)[Object.keys(sessions).length - 1]

    let attendanciesAndGradesById = {}
    Object.keys(sessions).forEach(key => {
      if (key === '0') return
      Object.keys(sessions[key].attendents_id).forEach(userKey => {
        const user = sessions[key].attendents_id[userKey]
        if (user === 0 || user === '0') return
        attendanciesAndGradesById = {
          ...attendanciesAndGradesById,
          [user.id]: {
            email: user.email,
            attendancies: attendanciesAndGradesById[user.id] && attendanciesAndGradesById[user.id].attendancies !== undefined
              ? attendanciesAndGradesById[user.id].attendancies + 1
              : 1,
            grades: []
          }
        }
      })
    })

    Object.keys(grades).forEach(userKey => {
      const user = grades[userKey]
      if (user === 0 || user === '0') return
      attendanciesAndGradesById = {
        ...attendanciesAndGradesById,
        [user.student_id]: {
          ...attendanciesAndGradesById[user.student_id],
          grades: attendanciesAndGradesById[user.student_id] && attendanciesAndGradesById[user.student_id].grades !== undefined
            ? [ ...attendanciesAndGradesById[user.student_id].grades, user.value ]
            : [ user.value ],
          attendancies: attendanciesAndGradesById[user.student_id] && attendanciesAndGradesById[user.student_id].attendancies !== undefined
            ? attendanciesAndGradesById[user.student_id].attendancies
            : 0,
          email: attendanciesAndGradesById[user.student_id] && attendanciesAndGradesById[user.student_id].email !== undefined
            ? attendanciesAndGradesById[user.student_id].email
            : 'anonymous@person.net',
        }
      }
    })

    Object.keys(attendanciesAndGradesById).forEach(key => {
      const classContainer = new Component({ type: 'li', attributes: { className: 'list-element' }, parent: classCardBodyUl })
      const accordion = new Component({ type: 'div', attributes: { className: 'accordion' }, parent: classContainer })
      new Component({ type: 'span', attributes: { className: 'bolder', innerHTML: attendanciesAndGradesById[key].email }, parent: accordion })
      
      const panel = new Component({ type: 'div', attributes: { className: 'accordion' }, parent: classContainer })
      const attendanciesContainer = new Component({ type: 'div', parent: panel })
      new Component({ type: 'span', attributes: { innerHTML: 'Attendances until now: ' }, parent: attendanciesContainer })
      new Component({ type: 'span', attributes: { className: 'bolder', innerHTML: attendanciesAndGradesById[key].attendancies }, parent: attendanciesContainer })
      const gradesContainer = new Component({ type: 'div', parent: panel })
      new Component({ type: 'span', attributes: { innerHTML: 'Grades until now: ' }, parent: gradesContainer })
      new Component({ type: 'span', attributes: { className: 'bolder', innerHTML: attendanciesAndGradesById[key].grades.join(', ') }, parent: gradesContainer })

      const form = new Form({ parent: panel }, addGradeCallback(key, className, session))
      new Input({ attributes: { type: 'text', name: 'grade' }, parent: form })
      new SubmitButton({ attributes: { className: 'btn-warning', value: 'Add grade' }, parent: form }, addGradeCallback(key, className, session))
      new SubmitButton({ attributes: { value: 'Kick student' }, parent: panel }, kickCallback(key, className, session))
    })

    attendants.setState({ text: `Attendants: ${Object.keys(attendanciesAndGradesById).length}` })
  }

  const startTimer = () => {

  }

  const render = async (prevState, { params }) => {
    document.body.innerHTML = ''

    const box = new Component({ type: 'div', attributes: { className: 'wrapper' } })
    const navbar = new Component({ type: 'div', attributes: { className: 'navbar' }, parent: box })
    const navbarLogoUl1 = new Component({ type: 'ul', attributes: { className: 'ul-left logo' }, parent: navbar })
    const navbarLogoLi1 = new Component({ type: 'li', parent: navbarLogoUl1 })
    const navbarLogoLiA1 = new Component({ type: 'a', attributes: { itemprop: 'url', onclick: () => router.push('teacher_dashboard', {}) }, parent: navbarLogoLi1 })
    new Component({ type: 'span', attributes: { innerHTML: 'Attend' }, parent: navbarLogoLiA1 })
    new Component({ type: 'span', attributes: { className: 'text-blue', innerHTML: 'N' }, parent: navbarLogoLiA1 })
    new Component({ type: 'span', attributes: { innerHTML: 'et' }, parent: navbarLogoLiA1 })
    const navbarLogoUl2 = new Component({ type: 'ul', parent: navbar })
    const navbarLogoLi2 = new Component({ type: 'li', parent: navbarLogoUl2 })
    new Component({ type: 'a', attributes: { itemprop: 'url', innerHTML: 'Dash', onclick: () => router.push('teacher_dashboard', {}) }, parent: navbarLogoLi2 })
    const navbarLogoLi3 = new Component({ type: 'li', parent: navbarLogoUl2 })
    new Component({ type: 'a', attributes: { itemprop: 'url', innerHTML: 'Logout', onclick: logout }, parent: navbarLogoLi3 })

    new Component({ type: 'h2', attributes: { className: 'center-text', innerHTML: params.class }, parent: box })
    new Component({ type: 'h3', attributes: { className: 'center-text', innerHTML: params.code || '' }, parent: box })
    clock = new Component({ type: 'h3', attributes: { className: 'center-text' }, parent: box })

    const classContainer = new Component({ type: 'div', attributes: { className: 'container container-center' }, parent: box })
    const classCard = new Component({ type: 'div', attributes: { className: 'card card-classes' }, parent: classContainer })
    const classCardHeader = new Component({ type: 'div', attributes: { className: 'card-header' }, parent: classCard })
    attendants = new Component({ type: 'h3', attributes: { className: 'h3', innerHTML: '' }, bindings: { innerHTML: 'text' }, state: { text: 'Attendants: ...' }, parent: classCardHeader })
    new Component({ type: 'hr', parent: classCard })
    const classCardBody = new Component({ type: 'div', attributes: { className: 'card-body' }, parent: classCard })
    classCardBodyUl = new Component({ type: 'ul', attributes: { className: 'list-elements' }, parent: classCardBody })

    const { session } = await fetchSession(params.class)
    displaySession(params.class, params.sessionId, session)
  }

  router.appendRoute({
    route,
    title,
    callback: render
  })

  return { render }
})()
