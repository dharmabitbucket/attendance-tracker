class SubmitButton extends Component {
  constructor (properties, submitCallback) {
    properties.type = 'input'
    super(properties)
    this.setAttributes({
      onsubmit: this.onSubmit(submitCallback),
      onclick: this.onSubmit(submitCallback),
      type: 'submit'
    })
  }

  onSubmit (submitCallback) {
    let formData = {}

    return event => {
      event.preventDefault()
      this.parent.children
        .filter(item => (item.type === 'input' || item.type === 'select') && item.attributes.name)
        .forEach(item => {
          formData[item.attributes.name] = item.state.value
        })
      submitCallback(formData)
    }
  }
}
