class Form extends Component {
  constructor (properties, submitCallback) {
    properties.type = 'form'
    super(properties)
    this.setAttributes({
      onsubmit: this.onSubmit(submitCallback)
    })
  }

  onSubmit (submitCallback) {
    let formData = {}

    return event => {
      event.preventDefault()
      this.children
        .filter(item => (item.type === 'input' || item.type === 'select') && item.attributes.name)
        .forEach(item => {
          formData[item.attributes.name] = item.state.value
        })
      submitCallback(formData)
    }
  }
}
