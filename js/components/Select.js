class Select extends Component {
  constructor (properties) {
    properties.type = 'select'
    super(properties)
    this.setAttributes({
      onchange: this.onChange()
    })
      .setState({
        value: 1
      })
      .setBindings({ value: 'value' })
  }

  onChange () {
    return event => {
      this.setState({
        value: event.target.value
      })
    }
  }
}
