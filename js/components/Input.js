class Input extends Component {
  constructor (properties) {
    properties.type = 'input'
    super(properties)
    this.setAttributes({
      oninput: this.onInput()
    })
      .setState({
        value: ''
      })
      .setBindings({ value: 'value' })
  }

  onInput () {
    return event => {
      this.setState({
        value: event.target.value
      })
    }
  }
}
